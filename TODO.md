# Choses qui restent  à faire au 29/05/2024

- [x] Mettre projet sur GitHub pour copie sur Replit [(ici)](https://github.com/olirwin/stages-seconde-template)
- [ ] Faire cheat sheet Replit
- [x] Faire cheat Sheet PyGame
- [ ] Proposer des idées d'amélioration de Python (avec des bouts de code)
- [ ] Décider à quel moment du projet iels passent en binomes (à noter dans le sujet)
- [ ] Est-ce qu'on crée les projets Replit pour tout le monde ?
