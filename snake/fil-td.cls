%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% FIL@Université de Lille class
% original file adapted and further developed by
%      Oliver Irwin <oliver.irwin@univ-lille.fr>
% % v0.1.1, 2024-04-08
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% original file:
% ens-ustl.sty
% LaTeX Package
% Version 0.26 (1997-12-19)
%
% Authors:
% Bruno Beaufils <beaufils@lifl.fr>
%
% Class license:
% LPPL v1.3c (http://www.latex-project.org/lppl)
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% -------------------------------------------------------------------------
% CLASS PRESENTATION
% -------------------------------------------------------------------------
\typeout{%
  *** Package: `fil-td' $Revision: 0.1.1 $ -- O. IRWIN ***}

% -------------------------------------------------------------------------
%	CLASS DEFINITION AND PARAMETERS
% -------------------------------------------------------------------------

\NeedsTeXFormat{LaTeX2e}
\newcommand{\classname}{fil-td}

\ProvidesClass{\classname}[2024/04/08 - TD FIL v0.1.1 FIL@ULILLE]
\providecommand{\baseclass}{article}
\RequirePackage{etoolbox}

%%% LANGUAGE SELECTION

\newbool{english}
\newbool{french}
\booltrue{french}
\newbool{nologo}
\newbool{nofluff}
\newbool{showsolution}
\newbool{bw}

\DeclareOption{english}{\booltrue{english}}
\DeclareOption{french}{\booltrue{french}}
\DeclareOption{nologo}{\booltrue{nologo}}
\DeclareOption{nofluff}{\booltrue{nofluff}}
\DeclareOption{showsolution}{\booltrue{showsolution}}
\DeclareOption{bw}{\booltrue{bw}}

%%% EXTRA OPTIONS

\DeclareOption*{\PassOptionsToClass{\CurrentOption}{\baseclass}}

\ifbool{english}{% Required to change language for document elements
    \typeout{English mode selected}
    \PassOptionsToClass{english}{\baseclass}
  }{}

\ifbool{french}{
    \typeout{French mode selected}
    \PassOptionsToClass{french}{\baseclass}
  }{}

%%% PROCESS OPTIONS
\ProcessOptions*

%%% LOAD BASE
\LoadClass[11pt, a4paper]{\baseclass}

% -------------------------------------------------------------------------
%	CLASS OPTIONS
% -------------------------------------------------------------------------

% -------------------------------------------------------------------------
%	REQUIRED PACKAGES
% -------------------------------------------------------------------------

\RequirePackage{lmodern}
\RequirePackage{FiraMono}

\RequirePackage[top=1.5cm,bottom=2cm,left=1.5cm,right=1.5cm]{geometry}

\RequirePackage{graphicx}
\RequirePackage[usenames,dvipsnames]{xcolor}
\RequirePackage{ifthen}
\RequirePackage{fontawesome5}
\RequirePackage{comment}
\RequirePackage{minted}
\RequirePackage{fancyhdr}
\RequirePackage{babel}
\RequirePackage{hyperref}

\RequirePackage{tcolorbox}
\tcbuselibrary{most, minted}

\RequirePackage{fontspec}
\setmainfont{Marianne}
\setsansfont{Marianne}
\setmonofont{Fira Mono}[Contextuals=Alternate]

\definecolor{ulille-darkblue}{HTML}{000037}
\definecolor{ulille-indigo}{HTML}{5862ED}
\definecolor{ulille-lightblue}{HTML}{7ABAFF}
\definecolor{ulille-darkgreen}{HTML}{002D2D}
\definecolor{ulille-green}{HTML}{32A68C}
\definecolor{ulille-lightgreen}{HTML}{89E0B1}
\definecolor{ulille-red}{HTML}{FC535C}
\definecolor{ulille-brown}{HTML}{2D0505}
\definecolor{ulille-pink}{HTML}{FFB4D2}
\definecolor{ulille-orange}{HTML}{FF6941}
\definecolor{ulille-yellow}{HTML}{FFD24B}
\definecolor{ulille-beige}{HTML}{FAF0E1}
\definecolor{ulille-lightgrey}{HTML}{EDEDED}
\definecolor{ulille-darkgrey}{HTML}{63656A}

% -------------------------------------------------------------------------
% HELPERS
% -------------------------------------------------------------------------

\NewDocumentCommand{\@getLanguageSymbol}{ m }{%
  \directlua{
    local s_tbl = {
      ["android"] = "\\faAndroid",
      ["angular"] = "\\faAngular",
      ["apple"] = "\\faApple",
      ["bash"] = "\\faTerminal",
      ["bootstrap"] = "\\faBootstrap",
      ["css"] = "\\faCss3*",
      ["docker"] = "\\faDocker",
      ["git"] = "\\faGit",
      ["html"] = "\\faHtml5",
      ["java"] = "\\faJava",
      ["js"] = "\\faJs",
      ["latex"] = "\\LaTeX",
      ["less"] = "\\faLess",
      ["markdown"] = "\\faMarkdown",
      ["node"] = "\\faNode",
      ["php"] = "\\faPhp",
      ["python"] = "\\faPython",
      ["react"] = "\\faReact",
      ["rust"] = "\\faRust",
      ["sass"] = "\\faSass",
      ["tex"] = "\\TeX",
      ["sql"] = "\\faDatabase",
      ["vue"] = "\\faVuejs",
    }
    local val = s_tbl["\luaescapestring{\unexpanded{#1}}"]
    if (val) then
      tex.print(val)
    else
      tex.print("\\faCode")
    end
  }
}

% -------------------------------------------------------------------------
% DOCUMENT THEME
% -------------------------------------------------------------------------

\colorlet{@solutionframecol}{\ifbool{bw}{ulille-darkgrey}{ulille-indigo}}

\NewDocumentCommand{\solutionframecol}{ m }{%
  \colorlet{@solutionframecol}{#1}
}

% -------------------------------------------------------------------------
% FIRST PAGE HEADER
% -------------------------------------------------------------------------

% Set variables

\NewDocumentCommand{\@school}{}{}
\NewDocumentCommand{\@course}{}{}
\NewDocumentCommand{\@teacher}{}{}
\NewDocumentCommand{\@subject}{}{}
\RenewDocumentCommand{\@title}{}{}

% User commands
\NewDocumentCommand{\school}{ m }{%
  \RenewDocumentCommand{\@school}{}{#1}%
}
\NewDocumentCommand{\course}{ m }{%
  \RenewDocumentCommand{\@course}{}{#1}%
}
\NewDocumentCommand{\teacher}{ m }{%
  \RenewDocumentCommand{\@teacher}{}{#1}%
}
\NewDocumentCommand{\subject}{ m }{%
  \RenewDocumentCommand{\@subject}{}{%
    \begin{tabular}{c}#1\end{tabular}%
  }%
}
\RenewDocumentCommand{\title}{ m }{%
  \RenewDocumentCommand{\@title}{}{%
    \begin{tabular}{c}#1\end{tabular}%
  }%
}

\global\let\author\relax
\global\let\@author\@empty

\newcounter{@yearbefore}
\setcounter{@yearbefore}{\year}
\newcounter{@yearafter}
\setcounter{@yearafter}{\year}

\RenewDocumentCommand{\@date}{}{%
  \ifthenelse{\month < 7}%
  {\addtocounter{@yearbefore}{-1}}
  {\addtocounter{@yearafter}{1}}
  \the@yearbefore--\the@yearafter%
}

\fancypagestyle{firststyle}{%
  \newgeometry{top=2.5cm,bottom=2cm,left=1.5cm,right=1.5cm}
   \fancyhf{}
   \fancyhead[C]{\large\@subject\par}
   \fancyhead[L]{\ifbool{nologo}{}{\hspace*{-1.25cm}\includegraphics[scale=0.175]{logo-CRIStAL}}}
   \fancyhead[R]{\raggedleft\par\@date\par\@course\par\@teacher}
   %\fancyfoot[C]{\thepage}
   \renewcommand{\headrulewidth}{0pt} % removes horizontal header line
}

\RenewDocumentCommand{\maketitle}{}{%
  \begin{center}
    \framebox{~\Large\bfseries\sffamily\@title~}
    \bigskip
    \hrule height 1pt
    \medskip
  \end{center}
  \thispagestyle{firststyle}
  % \ifbool{nologo}{%
  %     \begin{minipage}{0.2\textwidth}
  %       \@date
  %     \end{minipage}
  %     \begin{minipage}{.5\textwidth}
  %       \centering
  %       \large\@subject
  %     \end{minipage}
  %     \begin{minipage}{.25\textwidth}
  %       \raggedleft
  %       \@course
  %       \par
  %       \@teacher
  %     \end{minipage}
  %     \par
  %     \begin{center}
  %     \framebox{~\Large\bfseries\sffamily\@title~}
  %   \end{center}
  %   \smallskip
  %   \hrule
  %   \medskip
  % }{%
  %   \begin{center}
  %     \hspace*{-1.5cm}
  %   \begin{minipage}[][][c]{.33\textwidth}
  %     \centering
  %     \IfFileExists{logo_FIL.pdf}{%
  %       \includegraphics[width=\textwidth]{logo_FIL.pdf}
  %     }{%
  %       \ClassWarningNoLine{\classname}{No logo file found -- place
  %         `logo_FIL.pdf` file in folder or use `nologo` class option} }
  %   \end{minipage}
  %   \hfill
  %   \begin{minipage}{.25\textwidth}
  %     \centering
  %     \large\@subject
  %   \end{minipage}\framebox{~\Large\bfseries\sffamily\@title~}
  %   \hfill
  %   \begin{minipage}{.25\textwidth}
  %     \raggedleft
  %     \@date
  %     \par
  %     \@course
  %     \par
  %     \@teacher
  %   \end{minipage}
  %   \par
  %   \framebox{~\Large\bfseries\sffamily\@title~}
  % \end{center}
  % \smallskip
  % \hrule
  % \medskip
  % }
}



% -------------------------------------------------------------------------
% DOCUMENT ENVIRONMENTS
% -------------------------------------------------------------------------

% Set counters

\newcounter{@exercise}[section]
\newcounter{@question}[@exercise]
\newcounter{@subquestion}[@question]

% Set variables

\NewDocumentCommand{\@exercisename}{}{%
  \ifbool{english}{%
    Exercise%
  }{%
    Exercice%
  }
}

\NewDocumentCommand{\@questionname}{}{%
  Q.%
}

\NewDocumentCommand{\@subquestionname}{}{%
  Q.%
}

\NewDocumentCommand{\@solutionname}{}{%
  Solution%
}

% User commands

\NewDocumentCommand{\exercisename}{ m }{%
  \RenewDocumentCommand{\@exercisename}{}{#1}
}

\NewDocumentCommand{\questionname}{ m }{%
  \RenewDocumentCommand{\@questionname}{}{#1}
}

\NewDocumentCommand{\subquestionname}{ m }{%
  \RenewDocumentCommand{\@subquestionname}{}{#1}
}

\NewDocumentCommand{\solutionname}{ m }{%
  \RenewDocumentCommand{\@solutionname}{}{#1}
}

% Environments

\NewDocumentEnvironment{exercise}{ s O{} O{\@exercisename} }{%
  \sffamily\bfseries\underline{%
    #3%
    \IfBooleanF{#1}{%
      \stepcounter{@exercise}%
      \the@exercise%
    }%
  }
  : #2\\
  \mdseries\rmfamily
}{}

\NewDocumentEnvironment{exercise*}{ O{} O{\@exercisename} }{%
  \begin{exercise}*[#1][#2]
  }{
    \end{exercise}
  }

\NewDocumentEnvironment{question}{ s O{} O{\@questionname} }{%
  \noindent
  \sffamily\bfseries%
    #3%
    \IfBooleanF{#1}{%
      \stepcounter{@question}%
      ~\the@question%
    } #2
  \mdseries\rmfamily
}{~\\}

\NewDocumentEnvironment{question*}{ O{} O{\@questionname} }{%
  \begin{question}*[#1][#2]
  }{
    \end{question}
  }

  \NewDocumentEnvironment{subquestion}{ s O{} O{\@subquestionname} }{%
    ~\\\noindent
    \sffamily\bfseries%
    #3%
    \IfBooleanF{#1}{%
      \stepcounter{@subquestion}%
      ~\the@question.\the@subquestion%
    } #2
    \mdseries\rmfamily
  }{~\\}

  \NewDocumentEnvironment{subquestion*}{ O{} O{\@subquestionname} }{%
    \begin{subquestion}*[#1][#2]
    }{
    \end{subquestion}
  }

  \NewDocumentEnvironment{solutionstyle}{ O{} }{%
    \begin{tcolorbox}[
      title=\ifbool{nofluff}{\@solutionname}{\faCheck{} \@solutionname},
      fonttitle=\sffamily\bfseries,
      left=3pt,
      right=3pt,
      top=2pt,
      bottom=2pt,
      #1,
      breakable,
      enhanced,
      colframe=@solutionframecol]
    }{%
    \end{tcolorbox}
  }

% Handle showing of solutions only if option passed

  \ifbool{showsolution}{\NewDocumentEnvironment{solution}{ O{}
    }{\begin{solutionstyle}[#1]}{\end{solutionstyle}}}{
\excludecomment{solution}
}

% -------------------------------------------------------------------------
% CODE ENVIRONMENTS
% -------------------------------------------------------------------------

\NewDocumentCommand{\@codeblockname}{}{%
  Code%
}

\NewDocumentCommand{\codeblockname}{ m }{%
  \RenewDocumentCommand{\@codeblockname}{}{#1}%
}

\NewTCBListing{codeblock}{ m O{} }{%
	colback=ulille-lightgrey,
  colframe=ulille-darkgrey!55!black,
  title=\ifbool{nofluff}{\@codeblockname}{{\small \@getLanguageSymbol{#1}} \@codeblockname{}},
  fonttitle=\sffamily\bfseries,
  left=3pt,
  right=3pt,
  top=2pt,
  bottom=2pt,
  listing engine=minted,
  minted language=#1,
  minted options={autogobble,breaklines},
  listing only,
  breakable,
  enhanced,
  #2%
}

\NewTCBInputListing{\codefile}{ m m O{} }{%
	colback=ulille-lightgrey,
  colframe=ulille-darkgrey!55!black,
  title=\ifbool{nofluff}{\@codeblockname}{{\small \@getLanguageSymbol{#2}} \@codeblockname{}},
  fonttitle=\sffamily\bfseries,
  left=3pt,
  right=3pt,
  top=2pt,
  bottom=2pt,
  listing engine=minted,
  minted language=#2,
  minted options={autogobble, breaklines},
  listing only,
  listing file=#1,
  breakable,
  enhanced,
  #3%
}

\NewTotalTCBox{\cmd}{ O{ulille-darkgrey} v }{%
  enhanced,
  nobeforeafter,
  tcbox raise base,
  boxrule=0.4pt,
  top=0mm,
  bottom=0mm,
  right=0mm,
  left=4mm,
  arc=1pt,
  boxsep=2pt,
  before upper={\vphantom{dlg}},
  colframe=#1!50!black,
  coltext=#1!25!black,
  colback=#1!10!white,
  overlay={%
    \begin{tcbclipinterior}
      \fill[#1!75!blue!50!white] (frame.south west) rectangle
      node[text=white,font=\sffamily\bfseries\tiny,rotate=90] {CMD}
      ([xshift=4mm]frame.north west);
    \end{tcbclipinterior}
  }%
}{\ttfamily \small #2}

\NewTotalTCBox{\file}{ O{ulille-lightblue} v }{
  enhanced,
  nobeforeafter,
  tcbox raise base,
  boxrule=0.4pt,
  top=0mm,
  bottom=0mm,
  right=0mm,
  left=4mm,
  arc=1pt,
  boxsep=2pt,
  before upper={\vphantom{dlg}},
  colframe=#1!50!black,
  coltext=#1!25!black,
  colback=#1!10!white,
  overlay={
    \begin{tcbclipinterior}
      \fill[#1!75!blue!50!white] (frame.south west) rectangle
      node[text=white,font=\sffamily\bfseries\tiny,rotate=90] {FILE}
      ([xshift=4mm]frame.north west);
    \end{tcbclipinterior}
  }%
}{\ttfamily \small #2}


\endinput
