#!/bin/bash

ROOT_DIR=$HOME
INSTALL_FONT_DIR=$ROOT_DIR/.fonts

echo "copying font files to $INSTALL_FONT_DIR"

mkdir -vp $INSTALL_FONT_DIR
cp -ru fonts/* $INSTALL_FONT_DIR
fc-cache
