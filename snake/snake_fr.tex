\documentclass[french]{fil-td}

\input{preamble_fr}

\hypersetup{
  colorlinks,
  urlcolor=ulille-orange,
  linkcolor=ulille-green,
  pdfauthor=Oliver Irwin,
  pdftitle=Snake,
  pdfsubject=Projet Fil Rouge -- Stages Seconde 2024,
  pdfkeywords={Python, snake, jeu, projet, CRIStAL, CNRS}
}

\title{Projet Snake}
\subject{Stages Seconde 2024}
\teacher{\textsf{CRIStAL} - Commission Scolaire}

\BeforeBeginEnvironment{ideablock}{\savenotes}
\AfterEndEnvironment{ideablock}{\spewnotes}

% \BeforeBeginEnvironment{infoblock}{\savenotes}
% \AfterEndEnvironment{infoblock}{\spewnotes}

\BeforeBeginEnvironment{objectiveblock}{\savenotes}
\AfterEndEnvironment{objectiveblock}{\spewnotes}


\begin{document}

\maketitle

\begin{objectiveblock}[title={\small \faBullseye} Conseils]
  \begin{itemize}
  \item l'objectif est de vous faire tous travailler sur le projet, mais
    n'hésitez pas à discuter entre vous;
  \item toutes les notions utiles sont disponibles soit dans le document joint
    soit dans la documentation\footnote{\url{https://www.pygame.org/docs/}};
  \item la plupart des réponses se trouvent dans les questions;
  \item posez des questions \faLaughWink[regular]{}
  \end{itemize}
\end{objectiveblock}

\section*{Contexte}

Le jeu \descr{Snake} est un des jeux les plus populaires au monde\footnote{au
  moins à une certaine époque}. L'objectif du jeu est de contrôler un petit
serpent. Ce serpent peut se déplacer dans une zone de jeu bien définie et a pour
objectif de manger des pommes qui y sont placées aléatoirement. Chaque pomme
mangée fait grandir le serpent, qui devient alors plus difficile à contrôler. Le
jeu se termine si le serpent quitte la zone de jeu, si sa tête touche une autre
partie de son corps ou (dans le meilleur des cas), s'il remplit toute la surface
de jeu.

\begin{ideablock}
  Dans le squelette de code fourni se trouvent de multiples informations sur les
  endroits où écrire les morceaux de code. Utilisez ça à votre avantage!
\end{ideablock}

\subsection*{Organisation de la zone de jeu}

On définit une \descr{position} dans la zone de jeu par une paire de nombre qui
indiquent la distance au coin supérieur gauche: \emphase{(0,0)}. La première
coordonnée concerne la distance \descr{horizontale} et la seconde la distance
\descr{verticale}.

On définit une \descr{unité} comme un nombre de pixels qui vont permettre de
diviser la zone de jeux dans une grille. La première colonne (ou ligne) sera
\emphase{0}, la dernière aura l'indice \emphase{max - 1}.

Chaque \descr{élément de jeu} est composé d'un carré de taille \descr{unité},
identifié par la position de son coin supérieur gauche, qui correspondent à sa
colonne et sa ligne.

Le serpent commencera \descr{au milieu} de la zone de jeu.

Le serpent est composé de plusieurs carrés de la même couleur. Quand il se
déplace, on considère que la tête se déplace dans la direction donnée et que le
dernier carré disparaît.

Dans la \cref{fig:grid}, vous pouvez observer un exemple simplifié d'une grille
de \emphase{30x20}. Le serpent fait ici \emphase{5} carrés de long et la pomme
est à la position \emphase{(14, 5)}. Bien sûr, vous ne représenterez pas les
coordonnées à l'écran, elles sont juste là pour la visualisation.

\begin{figure}[htp]
  \centering
  \includegraphics{grid}
  \caption{Exemple de grille}
  \label{fig:grid}
\end{figure}

\clearpage
\step{Définition des variables}
\label{step:1}

Comme dans tout bon projet de code, on va commencer par définir quelques
variables qui vont nous permettre de gérer la zone de jeu proprement.

Vous aurez besoin de:

\begin{itemize}
\item une taille \descr{unite}, à régler à \emphase{10} pour le moment;
\item une taille \descr{largeur\_zone}, réglée à \emphase{60} unités;
\item une taille \descr{hauteur\_zone}, réglée à \emphase{40} unités;
\item une couleur \descr{couleur\_nourriture}\footnote{\label{fn:couleurs}il y a
  des exemples de couleurs dans le squelette, mais n'hésitez pas à utiliser les
  votres~!};
\item une couleur \descr{couleur\_serpent}\footnoteref[fn:couleurs];
\item une couleur \descr{couleur\_fond}\footnoteref[fn:couleurs].
\end{itemize}

\step{Préparation de la zone de jeu}

Dans cette étape, on se concentre sur la création et la préparation de la zone
de jeu.

\begin{enumerate}
\item mettez à jour le squelette pour construire une fenêtre de telle sorte à ce
  qu'elle utilise les variables de largeur et de hauteur de zone que vous avez
  définies;
\item ajoutez une couleur de fond à la fenêtre;
\item écrivez le corps de la fonction \descr{dessiner\_carre}. Cette fonction
  prend 3 paramètres: \emphase{x}, \emphase{y} et \emphase{couleur}. Elle
  dessinera un carré de taille \descr{unite} à la position \emphase{(x, y)}.
\end{enumerate}

\begin{ideablock}
  N'hésitez pas à tester votre fonction dans votre grille pour vérifier que vous
  avez bien un carré de la bonne couleur au bon endroit.
\end{ideablock}

\step{Une pincée d'aléatoire}

On veut pouvoir faire apparaître la nourriture à des endroits aléatoires de la
grille, sinon le jeu est trop facile. Pour pouvoir faire ça, il faut qu'on soit
en mesure de générer des positions aléatoires.

\begin{enumerate}
\item Écrivez une fonction \descr{entier\_aleatoire} qui prend un paramètre
  \emphase{max} et qui renvoie un entier aléatoire entre \emphase{0} et
  \emphase{max} (exclus).
\item Écrivez une fonction \descr{position\_aleatoire} qui renvoie une position
  (donc un couple de coordonnées) qui doit être \descr{valide}, c'est à dire qui
  est dans la zone de jeu. Vous devriez utiliser la fonction que vous venez de
  définir et les variables de l'\cref{step:1}.
\end{enumerate}

\begin{ideablock}
  La structure de données la plus efficace pour représenter ce couple d'entiers
  est le \emphase{tuple}
\end{ideablock}

\step{Afficher plusieurs carrés}

Le serpent va vite être constitué de plusieurs carrés. On va donc devoir définir
une fonction \descr{dessine\_liste\_carres} qui prend en paramètres:

\begin{itemize}
\item une liste de positions;
\item une couleur.
\end{itemize}

et qui dessine pour chaque position dans la liste un carré. Vous utiliserez bien
sûr la fonction que vous avez définie lors de l'\cref{step:1}. Tous les carrés
de la liste seront de la même couleur.

\step{Variables des éléments de jeu}

On va redéfinir quelques variables supplémentaires, qui cette fois vont nous
aider à stocker des informations importantes sur la partie. Vous allez avoir
besoin de:

\begin{itemize}
\item une liste \descr{positions\_serpent}, qui représente la liste des
  positions des carrés constituant le serpent. Au départ, vous pouvez n'y mettre
  qu'un seul élément qui est le milieu de la zone de jeu.
\item un booléen \descr{fin\_jeu}, qui nous indiquera que la partie est
  terminée. Bien sûr, cette variable vaut \emphase{False} au départ.
\item la direction du serpent \descr{direction\_serpent}, chaîne vide au départ.
\item la position de la pomme \descr{position\_pomme}, qui sera initialement
  choisie en appelant une fonction bien choisie.
\end{itemize}

\step{Un tour de jeu}

Un tour de jeu est constitué de plusieurs actions, que l'on va décrire
maintenant:

\begin{enumerate}
\item appeler la fonction \descr{dessine\_carre} pour faire apparaître la pomme
  au bon endroit;
\item appeler la fonction \descr{afficher\_message} pour faire apparaître le
  score en haut à gauche. On définit le score comme étant la longueur du serpent
  moins 1.
\item appeler la fonction \descr{dessine\_liste\_carres} pour afficher le
  serpent.
\item utiliser la variable \descr{fin\_jeu} dans la boucle principale.
\end{enumerate}

\step{Les directions}

Notre serpent va pouvoir se déplacer dans plusieurs directions. On va créer une
fonction \descr{direction}, qui va prendre deux paramètres: la touche sur
laquelle on appuie et la direction du serpent actuelle. On se base ensuite sur
la touche pressée pour changer la direction. Si c'est:

\begin{itemize}
\item \emphase{HAUT}, alors la coordonnée \emphase{x} ne change pas et
  \emphase{y} augmente de 1;
\item \emphase{BAS}, alors on ne touche pas à \emphase{x} et on décroît
  \emphase{y} de 1;
\item \emphase{GAUCHE}, alors \emphase{x} decroît de 1 et \emphase{y} ne change
  pas;
\item \emphase{DROITE}, alors \emphase{x} augmente de 1 et \emphase{y} ne change
  pas.
\end{itemize}

La fonction renverra un tuple d'entiers qui représente les changements pour
\emphase{x} et \emphase{y}. Si une autre touche est pressée, alors on ne change
rien bien sûr.

\step{Réagir à un évènement}

Deux évènements principaux peuvent arriver pendant la partie. Il faudra gérer ce
qu'il se passe pour chaque.

\begin{enumerate}
\item Le joueur appuie sur la bouton de sortie. Dans ce cas, la partie se
  termine, une variable doit changer.
\item Le joueur appuie sur une touche. Dans ce cas, la direction du serpent peut
  changer. Utilisez une fonction définie précédemment pour gérer ceci.
\end{enumerate}

\begin{ideablock}
  Vous pouvez utiliser \emphase{event.type} pour obtenir le type d'un évènement.
\end{ideablock}

\step{Calcul de la nouvelle position}

Écrivez une fonction \descr{nouvelle\_position} qui prend en paramètre le
serpent et la direction actuelle. La fonction renvoie la nouvelle position de la
tête du serpent.

Vous calculerez la nouvelle coordonnée à partir de la direction. Pour faire plus
simple, vous pourrez ajouter la nouvelle position à la fin de la liste des
positions du serpent, mais gardez en tête que c'est le dernier élément de cette
liste qui représentera la tête à chaque fois.

\step{Respecter la zone}

On ne veut pas que notre serpent puisse quitter la zone (ça pourrait même être
un motif de fin de partie). Écrivez donc une fonction \descr{dans\_zone} qui
renvoie \emphase{True} si la position est dans la zone de jeu et \emphase{False}
sinon.

\begin{ideablock}
  Utilisez les variables que vous avez définies à l'\cref{step:1} pour
  déterminer si on est dans la zone!
\end{ideablock}

\step{Déplacer le serpent}

Il faut maintenant déterminer la nouvelle position de la tête et vérifier deux
choses :

\begin{enumerate}
\item si la tête du serpent est dans la zone de jeu, alors elle est ajoutée au
  serpent, sinon, la partie est finie;
\item si la tête du serpent est sur la pomme, alors il faut déterminer une
  nouvelle position valide pour la nourriture. Sinon, alors on retire de la
  liste la position de la queue du serpent.
\end{enumerate}

\end{document}

%%% Local Variables:
%%% mode: latex
%%% TeX-master: t
%%% TeX-engine: luatex
%%% End:
