# RUBIKA - SNAKE GAME

# imports
import pygame
import random

# colors
blanc = (255, 255, 255)
jaune = (255, 255, 102)
noir = (0, 0, 0)
rouge = (213, 50, 80)
vert = (0, 255, 0)
bleu = (50, 153, 213)

# Step 1: Defining the window's graphical variables
unite = 10
largeur_zone = 60
hauteur_zone = 30
couleur_nourriture = rouge
couleur_serpent = vert
couleur_fond = blanc


# Step 2.3: Showing a square in the window
def dessiner_carre(x: int, y: int, colour: tuple):
    pygame.draw.rect(
        console,
        colour,
        [x, y, unite, unite]
    )


# Step 4: Showing multiple squares from a list of positions and a colour
def dessiner_liste_carres(liste_positions: list, couleur: tuple):
    for position in liste_positions:
        dessiner_carre(position[0], position[1], couleur)


# Step 3.1: Get a random number between 0 and max (excluded)
def entier_aleatoire(valeur_max: int) -> int:
    return random.randint(0, valeur_max-1)


# Step 3.2: Get random coordinates in the zone
def position_aleatoire() -> tuple:
    position_0 = entier_aleatoire(largeur_zone) * unite
    position_1 = entier_aleatoire(hauteur_zone) * unite
    return position_0, position_1


# Print a message at the top left of the screen in Comic Sans MS, size 20
def affiche_message(message: str, couleur: tuple):
    console.blit(pygame.font.SysFont("comicsansms", 20).render(message, True, couleur), [0, 0])


# Step 7: Direction according to the pressed key
def actualiser_direction(keys: pygame.key, direction_actuelle: tuple) -> tuple:
    if keys[pygame.K_UP]:
        direction_future = (0, -1)
    elif keys[pygame.K_DOWN]:
        direction_future = (0, 1)
    elif keys[pygame.K_LEFT]:
        direction_future = (-1, 0)
    elif keys[pygame.K_RIGHT]:
        direction_future = (1, 0)
    else:
        direction_future = direction_actuelle
    return direction_future


# Step 9: Getting the new position in the current direction
def nouvelle_position(serpent, direction_actuelle):
    position = [serpent[-1][0] + direction_actuelle[0]*unite, serpent[-1][1] + direction_actuelle[1]*unite]
    return position


# Step 10: Checking is the position is in the game area
def dans_zone(position):
    x = position[0]
    y = position[1]
    if x < 0 or x >= largeur_zone*unite:
        return False
    if y < 0 or y >= hauteur_zone*unite:
        return False
    return True


# One game turn
def tour_jeu(serpent, fin, direction_actuelle, pomme):

    # Step 6.1: Show the food
    dessiner_carre(pomme[0], pomme[1], couleur_nourriture)

    # Step 6.2: Show the score
    affiche_message(str(len(serpent)-1), noir)

    # Step 6.3: Show the snake
    dessiner_liste_carres(serpent, couleur_serpent)

    # Update display
    pygame.display.update()

    # Retrieve pressed keys
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            fin = True

    keys = pygame.key.get_pressed()
    direction_future = actualiser_direction(keys, direction_actuelle)
    position_future = nouvelle_position(serpent, direction_future)
    if dans_zone(position_future):
        serpent.append(position_future)
        direction_actuelle = direction_future
    else:
        fin = True
    if tuple(serpent[-1]) == pomme:
        pomme = position_aleatoire()
    else:
        serpent = serpent[1:]
    return serpent, fin, direction_actuelle, pomme


# Initialisation of the game window ==> do not delete
pygame.init()

# Step 2.1: replace 100,100 by using the correct variables
console = pygame.display.set_mode((largeur_zone*unite, hauteur_zone*unite))

# Set window title
pygame.display.set_caption("Snakeeeeee")

# Step 5: Game elements variables
serpent = [[largeur_zone*unite//2, hauteur_zone*unite//2]]
fin_jeu = False
direction_serpent = (0, 0)
position_pomme = position_aleatoire()

# the clock to make the time move
clock = pygame.time.Clock()
tick = 20

# Step 6.4: As long as the game is not over
while fin_jeu is False:
    # Step 2.2: colour the background
    console.fill(blanc)
    # Update the elements
    serpent, fin_jeu, direction_serpent, position_pomme = tour_jeu(serpent, fin_jeu, direction_serpent, position_pomme)
    clock.tick(tick)

# End of game
print(f"Game Over, score: {str(len(serpent))}")
pygame.quit()
quit()
